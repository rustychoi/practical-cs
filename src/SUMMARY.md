# Summary

[Practical CS](./title-page.md)
[Introduction](./ch00-00-introduction.md)

- [Merge Sort](./ch01-00-merge-sort.md)
    - [Merge](./ch01-01-merge.md)
    - [Review of Recursion](./ch01-02-recursion.md)
    - [Putting it all together](./ch01-03-all-together.md)
