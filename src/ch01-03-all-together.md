# Putting it all together

The intuition behind merge sort is to take the problem of sorting a list, and reduce it to the simpler problem of merging two sorted lists, which we know can be solved by `merge`.

A high level overview of merge sort goes like this:
1. say that we want to sort a list `xs`
1. split `xs` down the middle into two lists of roughly equal sizes, and call them `xs_left` and `xs_right`
    ```python
    # for example if `xs = [1, 9, 3, 7]`
    xs_left = [1, 9]
    xs_right = [3, 7]
    ```
1. sort `xs_left`, call it `xs_left_sorted`
1. sort `xs_right`, call in `xs_right_sorted`
1. return `merge(xs_left_sorted, xs_right_sorted)` as the sorted version of `xs`

Without discussing in detail how exactly `xs_left` and `xs_right` are sorted, can we agree that following the above steps will sort `xs`? Afterall, if we are somehow able to sort `xs_left` and `xs_right`, then `merge(xs_left_sorted, xs_right_sorted)` will not only be sorted but also contain all the elements that used to be in `xs`. That, by definition, will be the sorted version of `xs`.

Now let's explore the code for merge sort:

```python
def merge_sort(xs):
    # BASE CASE
    #
    # if the length is `< 2`, then it's either length 0 or 1, which means that
    # `xs` is already sorted
    if len(xs) < 2:
        return xs
    # RECURSIVE CASE
    #
    # sort the left half, sort the right half, and `merge` the 2
    else:
        #
        middle_index = len(xs) // 2
        xs_left = xs[:middle_index]
        xs_right = xs[middle_index:]

        #
        xs_left_sorted = merge_sort(xs_left)
        xs_right_sorted = merge_sort(xs_right)

        #
        merged_list = merge(xs_left_sorted, xs_right_sorted)
        return merged_list

print(merge_sort([]))                # prints `[]`
print(merge_sort([1]))               # prints `[1]`
print(merge_sort([1, 4, 2, 9, 10]))  # prints `[1, 2, 4, 9, 10]`
```

---

## Intuition

I like to think about recursive algorithms in the following way: ___forget about the base case trust/assume that recursive calls work___. Let me explain what I mean by this.

When I develop software I like to write pseudo-code first, and by iteratively revising them. So for merge sort, I would come up with something like the code below as my first draft:

```python
def merge_sort(xs):
    # split xs into halves
    xs_left  = left half of xs
    xs_right = right half of xs

    # sort the halves
    xs_left_sorted  = merge_sort(xs_left)
    xs_right_sorted = merge_sort(xs_right)

    #
    return merge(xs_left_sorted, xs_right_sorted)
```

Now this pseudo code above is not fully correct yet: it will run forever because the recursion never stops. But it illustrates some important ideas:

1. I think about the ___recursive case___ first, and only the recursive case
1. I assume/trust that the recursive calls used in the recursive case will somehow magically work, and don't think about the base case

If we trust my advice and assume that the recursive calls to `merge_sort` applied to `xs_left` and `xs_right` somehow magically work, then the above pseudo-code becomes super easy to reason about. We sort the left half, we sort the right, and we merge the two sorted lists! Done! How simple!

Now all that's left for us to do is to implement the base case in order to make the recursive case work. With each successive recursive calls to `merge_sort`, the recursive case splits the list into halves. This means that from the perspective of `merge_sort`, `xs` will become smaller and smaller until it becomes size-0 or size-1.

So as a base case, we simply return `xs` without modification if its length is `0` or `1` because ___all length-0 and length-1 lists are already sorted___, by definition. This provides a solid base case that all recursive calls to `merge_sort` are bound to hit.

---

## Tracing Recursion

Let's modify the code for `merge_sort` slightly, in order to see how the recursion is actually working:

```python
def merge_sort(xs):
    print('merge_sort(', xs, ')')

    if len(xs) < 2:
        print('  base case: return ', xs)
        return xs
    else:
        #
        middle_index = len(xs) // 2
        #
        xs_left = xs[:middle_index]
        xs_right = xs[middle_index:]

        #
        print('  recursive case: sort and merge', xs_left, 'and', xs_right)
        xs_left_sorted = merge_sort(xs_left)
        xs_right_sorted = merge_sort(xs_right)

        merged_list = merge(xs_left_sorted, xs_right_sorted)
        print('  recursive case: return', merged_list)
        return merged_list

# feel free change this line to try out a different list!
merge_sort([1, 4, 2, 9, 10])
```

Running the code above prints:
```txt
merge_sort( [1, 4, 2, 9, 10] )
  recursive case: sort and merge [1, 4] and [2, 9, 10]
merge_sort( [1, 4] )
  recursive case: sort and merge [1] and [4]
merge_sort( [1] )
  base case: return  [1]
merge_sort( [4] )
  base case: return  [4]
  recursive case: return [1, 4]
merge_sort( [2, 9, 10] )
  recursive case: sort and merge [2] and [9, 10]
merge_sort( [2] )
  base case: return  [2]
merge_sort( [9, 10] )
  recursive case: sort and merge [9] and [10]
merge_sort( [9] )
  base case: return  [9]
merge_sort( [10] )
  base case: return  [10]
  recursive case: return [9, 10]
  recursive case: return [2, 9, 10]
  recursive case: return [1, 2, 4, 9, 10]
```

---

## Overview

Write recursive algorithms by following these steps:

1. write the _recursive case_ first, by trusting and assuming that recursive calls magically work
1. write the _base case_ to make the recursive case work
