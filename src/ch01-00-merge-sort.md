# Merge Sort

In this chapter we will learn about the [merge sort](https://en.wikipedia.org/wiki/Merge_sort) algorithm. 

Merge sort is one of the simplest and the most popular sorting algorithms, and is one of the first algorithms you will learn about in most computer science curriculum.

## Definition

A [sorting algorithm](https://en.wikipedia.org/wiki/Sorting_algorithm) is one that sorts a sequence of items into a certain order. The most common example of sorting is to sort a sequence of numbers into an ascending order:
```python
# sorted in an ascending order
ascending_order = [1, 3, 4, 5, 7]

# NOT sorted in an ascending order
descending_order = [7, 5, 4, 3, 1]
neither_order = [1, 4, 3, 7, 5]
```

> From this point on, we will assume that `sorted` means `sorted in an ascending order`.
