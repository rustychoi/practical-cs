# Merge

Before we can understand merge sort, we have to understand an operation called `merge` which plays a crucial role in merge sort.

`merge` solves the following problem: given 2 ___sorted___ sequences `list_a` and `list_b`, merge them into a sorted sequence `merged_list` such that:
1. `merged_list` contains ___all___ of the elements in `list_a` and `list_b`
1. `merged_list` is sorted

For example:
```python
list_a = [1, 4, 5]
list_b = [2, 3, 8]
merged_list = merge(list_a, list_b)

# should print `[1, 2, 3, 4, 5, 8]`
print(merged_list)
```
---

## Python Implementation

Below is an implementation of `merge` in Python:

```python
def merge(list_a, list_b):
    # list that holds the result
    merged_list = []

    # indices to keep track of where we are in each list
    # `index_a` is for `list_a` and `index_b` is for `list_b`
    index_a = 0
    index_b = 0

    # LOOP 1: see explanation below
    while index_a < len(list_a) and index_b < len(list_b):
        elem_a = list_a[index_a]
        elem_b = list_b[index_b]

        # 1. append the smaller element to `merged_list`
        # 2. increment the corresponding index to move forward in that list
        if elem_a < elem_b:
            merged_list.append(elem_a)
            index_a += 1
        else:
            merged_list.append(elem_b)
            index_b += 1

    # now all that's left to do is to make sure that we add any remaining
    # elements in either `list_a` or `list_b` to `merged_list`

    # LOOP 2:
    # if `list_a` is the list that is non-empty, just go through it 
    # and append any remaining elements
    while index_a < len(list_a):
        elem_a = list_a[index_a]
        merged_list.append(elem_a)
        index_a += 1

    # LOOP 3:
    # do the same in case it's `list_b` that's non-empty
    while index_b < len(list_b):
        elem_b = list_b[index_b]
        merged_list.append(elem_b)
        index_b += 1

    # return the merged list
    return merged_list
```

---

## Correctness

Let's talk about the three `while` loops in the code above, to understand why `merge` solves the problem correctly.

### Loop 1

What we are doing in this loop is essentially taking advantange of the fact that `list_a` and `list_b` are already sorted. 

Imagine that you have `list_a` and `list_b` written down on paper, and you start by placing one finger on the first item in `list_a`, and another finger on the first item of `list_b`. You then do the following:
1. compare the two numbers that you have your fingers on
1. call the smaller number `x`
    1. write `x` down on paper
    1. move the finger that's placed over `x` and move it to the number to its right, which is the next smallest number in that list that is greater than or equal to `x`
1. go back to step 1, and repeat the process until you hit the end of either `list_a` or `list_b`

Let's visualize this by going through an example:

1. let `list_a = [1, 3, 5, 7]`
1. let `list_b = [2, 4, 4]`
1. the underlined numbers represents the numbers that you have your fingers on

The table below shows different variables in each iteration of the `while` loop:

| iteration | `merged` | `list_a` | `list_b` | `index_a` | `index_b` |
| :-: | :- | :- | :- | :-: | :-: |
| 1 | \\( [] \\) | \\( [\underline{1}, 3, 5, 7] \\) | \\( [\underline{2}, 4, 4] \\) | 0 | 0 |
| 2 | \\( [1] \\) | \\( [1, \underline{3}, 5, 7] \\) | \\( [\underline{2}, 4, 4] \\) | 1 | 0 |
| 3 | \\( [1, 2] \\) | \\( [1, \underline{3}, 5, 7] \\) | \\( [2, \underline{4}, 4] \\) | 1 | 1 |
| 4 | \\( [1, 2, 3] \\) | \\( [1, 3, \underline{5}, 7] \\) | \\( [2, \underline{4}, 4] \\) | 2 | 1 |
| 5 | \\( [1, 2, 3, 4] \\) | \\( [1, 3, \underline{5}, 7] \\) | \\( [2, 4, \underline{4}] \\) | 2 | 2 |
| 6 | \\( [1, 2, 3, 4, 4] \\) | \\( [1, 3, \underline{5}, 7] \\) | \\( [2, 4, 4] \\) | 2 | 3 |

### Loop 2 and Loop 3

Now that we have gone through one of the lists completely, we have to grab the list that we have not gone through entirely, and add all of those to the merged list:


| iteration | `merged` | `list_a` | `list_b` | `index_a` | `index_b` |
| :-: | :- | :- | :- | :-: | :-: |
| 7 | \\( [1, 2, 3, 4, 4, 5] \\) | \\( [1, 3, 5, \underline{7}] \\) | \\( [2, 4, 4] \\) | 3 | 3 |
| 8 | \\( [1, 2, 3, 4, 4, 5, 7] \\) | \\( [1, 3, 5, 7] \\) | \\( [2, 4, 4] \\) | 4 | 3 |

---

## Runtime Complexity

`merge` runs in linear time; more specifically \\( O(m + n) \\), where \\(m\\) is the length of `list_a` and \\(n\\) is the length of `list_b`.

This is because each of the while loops can run at most \\(m + n\\) times.

---

## Other Thoughts

1. the problem that `merge` solves is simpler than the problem of sorting a list
1. this `merge` operation may seem irrelevant to the task of sorting a list, but it actually plays a critical role in merge sort
