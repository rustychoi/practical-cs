# Review of Recursion


Recursive algorithms have a _base case_ and a _recursive case_. We can generalize and think of recursive algorithms as having the following form:

```python
def f(x):
    if base_case_condition:
        base_case
    else:
        recursive_case
```

1. _base case_: code within `f` that can solve `f`'s problem ___without___ any recursive calls to `f`
1. _recursive case_: code within `f` that makes recursive calls to `f`

While not all recursive algorithms follow this pattern, we will assume that they do, in order to build intuition around recursive algorithms.
